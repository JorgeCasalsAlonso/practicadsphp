<?php
    require '../views/header.php';
?>
<div id="content">
    <p>
        Bienvenido a la p&aacute;gina principal,
        <?php if ($_SESSION['username'] == '') : ?>
            invitado
        <?php else : ?>
            <?php echo $_SESSION['username'] ?>
        <?php endif ?>
    </p>
</div>

<?php
    require '../views/footer.php';
?>
