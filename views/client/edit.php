<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Editar cliente</h1>
    <form action=<?php echo "/client/update/" . $client['id']?> method="post">
        <table>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" name="name" value=<?php echo $client['name'] ?>></td>
            </tr>
            <tr>
                <td>Direcci&oacute;n:</td>
                <td><input type="text" name="address" value=<?php echo $client['address'] ?>></td>
            </tr>
            <tr>
                <td>Tel&eacute;fono:</td>
                <td><input type="text" name="phone" value=<?php echo $client['phone'] ?>></td>
            </tr>
            <tr/>
            <tr>
                <td>Cr&eacute;dito:</td>
                <td><input type="text" name="credit" value=<?php echo $client['credit'] ?>></td>
            </tr>
        </table>
        <input type="submit" value="Modificar">
    </form>
</div>

<?php
    require '../views/footer.php';
?>
