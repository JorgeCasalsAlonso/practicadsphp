<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Lista de clientes</h1>
    <div style="float: left">
        <a href=<?php echo '/client/new/'?>>Nuevo cliente</a>
    </div>
    <div style="float: right">
        <a href=<?php echo '/client/search/'?>>Buscar cliente</a>
    </div>
    <?php if ($pages==0) : ?>
        <p>No hay elementos que mostrar.</p>
    <?php else : ?>
        <table>
            <?php foreach ($rows as $i => $row) : ?>
                <?php if ($i == 0) : ?>
                    <thead>
                        <tr>
                            <?php foreach ($row as $key => $value) : ?>
                                <th><?php echo ucfirst($key) ?></th>
                            <?php endforeach ?>
                            <th colspan="2">Acciones</th>
                        </tr>
                    </thead>
                <?php endif ?>
                <tr>
                    <?php foreach ($row as $key => $value) : ?>
                        <td><?php echo $value ?></td>
                    <?php endforeach ?>
                    <td><a href=<?php echo '/client/edit/' . $row['id'] ?>>Editar</a></td>
                    <td><a href=<?php echo '/client/delete/' . $row['id'] ?>>Borrar</a></td>
                </tr>
            <?php endforeach ?>
        </table>
        <a href=<?php echo "/client/index/1" ?>>&laquo</a>&nbsp;
        <?php if ($page>1) : ?>
            <a href=<?php echo "/client/index/" . ($page-1) ?>>&lt;</a>&nbsp;
        <?php endif ?>
        <?php if ($pages>$page) : ?>
            <?php for ($i = 1; $i<=$pages; $i++) : ?>
                <a href=<?php echo "/client/index/" . $i ?>><?php echo $i ?></a>&nbsp;
            <?php endfor ?>
        <?php else : ?>
            <?php for ($i = 1; $i<=$page; $i++) : ?>
                <a href=<?php echo "/client/index/" . $i ?>><?php echo $i ?></a>&nbsp;
            <?php endfor ?>
        <?php endif ?>
        <?php if ($page<$pages) : ?>
            <a href=<?php echo "/client/index/" . ($page+1) ?>>&gt;</a>
        <?php endif ?>
        <a href=<?php echo "/client/index/" . ($pages) ?>>&raquo</a>&nbsp;
    <?php endif ?>
</div>

<?php
    require '../views/footer.php';
?>
