<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Nuevo cliente</h1>
    <form action="/client/insert" method="post">
        <table>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td>Direcci&oacute;n:</td>
                <td><input type="text" name="address"></td>
            </tr>
            <tr>
                <td>Tel&eacute;fono:</td>
                <td><input type="text" name="phone"></td>
            </tr>
            <tr/>
            <tr>
                <td>Cr&eacute;dito:</td>
                <td><input type="text" name="credit"></td>
            </tr>
        </table>
        <input type="submit" value="Dar de alta">
    </form>
</div>

<?php
    require '../views/footer.php';
?>
