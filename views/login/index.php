<?php
    require '../views/header.php';
?>
<div id="content">
    <?php if ($_SESSION['username'] != '') : ?>
        <p>Ya has iniciado sesión, <?php echo $_SESSION['username'] ?>.</p>
    <?php else : ?>
        <h1>Inicio de sesi&oacute;n</h1>
        <form action="/Login/in" method="post">
            <table>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" name="login"></td>
                </tr>
                <tr/>
            </table>
            <input type="submit" value="Iniciar sesi&oacute;n">
        </form>
    <?php endif ?>
</div>

<?php
    require '../views/footer.php';
?>
