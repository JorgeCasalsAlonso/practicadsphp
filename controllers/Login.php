<?php

namespace Practica\Controller;

class Login
{
    public function __construct()
    {
        //echo "Constructor de Login";
    }

    public function index()
    {
        require '../views/login/index.php';
    }

    public function in()
    {
        $_SESSION['username'] = $_POST['login'];
        header('Location: /');
    }

    public function out()
    {
        session_unset();
        header('Location: /');
    }
}
