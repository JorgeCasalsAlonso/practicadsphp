<?php

namespace Practica\Controller;

require_once '../app/Controller.php';
require_once '../models/Client.php';

class Client extends \Practica\App\Controller
{
    public function __construct()
    {
        parent::__construct();
        //echo '<br/>';
        //echo "En el controlador Client";
        $this->_model = new \Practica\Model\Client;
        //echo '<pre>';
        //var_dump($this);
        //echo '</pre>';
    }

    public function index($page = 5)
    {
        if ($_POST['name'] == ""){
            $rows = $this->_model->get($page);
            $pages = $this->_model->getPageCount();
        } else {
            $rows = $this->_model->search($_POST['name']);
            $page = 1;
            $pages = 1;
        }
        require '../views/client/index.php';
    }

    public function new()
    {
        //formulario de alta de datos
        require '../views/client/new.php';
    }
    public function insert()
    {
        //inserción de datos nuevos
        try {
            $this->_model->insert($_POST["name"], $_POST["address"], $_POST["phone"], $_POST["credit"]);
            header('Location: /client/index/1');
        } catch (\Exception $e) {
            $error = new \Practica\Controller\Error();
            $error->index($e);
        }
        
    }
    public function delete($id)
    {
        //eliminación de datos
        try {
            $this->_model->delete($id);
            header('Location: /client/index/1');
        } catch (\Exception $e) {
            $error = new \Practica\Controller\Error();
            $error->index($e);
        }
    }
    public function edit($id)
    {
        //formulario de edición
        try {
            $client = $this->_model->select($id);
            require '../views/client/edit.php';
        } catch (\Exception $e) {
            $error = new \Practica\Controller\Error();
            $error->index($e);
        }
    }

    public function update($id)
    {
        //actualización de datos una vez editados
        try {
            $this->_model->update($id, $_POST["name"], $_POST["address"], $_POST["phone"], $_POST["credit"]);
            header('Location: /client/index/1');
        } catch (\Exception $e) {
            $error = new \Practica\Controller\Error();
            $error->index($e);
        }
    }

    public function search(){
        require '../views/client/search.php';
    }
}
