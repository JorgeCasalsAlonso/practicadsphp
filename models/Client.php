<?php
namespace Practica\Model;

require_once '../app/Model.php';
class Client extends \Practica\App\Model
{
    public function __construct()
    {
        parent::__construct();
        //echo '<br/>';
        //echo 'En el modelo Client';
    }
    public function get($page)
    {
        $offset = ($page-1) * $this::PAGE_SIZE;
        $sql = "select id, name, address, phone, credit from client limit " . $offset . "," . $this::PAGE_SIZE;
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getPageCount()
    {
        $sql = "select count(id) from client";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchColumn();
        return intval(ceil($result / $this::PAGE_SIZE));
    }

    public function insert($post_name, $post_address, $post_phone, $post_credit)
    {
        if ($post_name != "" && $post_address != "" && is_numeric($post_phone) && is_numeric($post_credit)) {
            $sql = "insert into client (name, address, phone, credit) values(:name, :address, :phone, :credit)";
            $stmt = $this->_pdo->prepare($sql);
            $stmt->bindValue(':name', $post_name, \PDO::PARAM_STR);
            $stmt->bindValue(':address', $post_address, \PDO::PARAM_STR);
            $stmt->bindValue(':phone', $post_phone, \PDO::PARAM_STR);
            $stmt->bindValue(':credit', $post_credit, \PDO::PARAM_STR);
            $stmt->execute();
        }
    }

    public function select($post_id)
    {
        $sql = "select id, name, address, phone, credit from client where id  = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':id', $post_id, \PDO::PARAM_INT);
        $stmt->execute();
        $client = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $client;
    }

    public function update($post_id, $post_name, $post_address, $post_phone, $post_credit)
    {
        if ($post_name != "" && $post_address != "" && is_numeric($post_phone) && is_numeric($post_credit)) {
            $sql = "update client set name = :name, address = :address, phone = :phone, credit = :credit where id = :id";
            $stmt = $this->_pdo->prepare($sql);
            $stmt->bindValue(':id', $post_id, \PDO::PARAM_INT);
            $stmt->bindValue(':name', $post_name, \PDO::PARAM_STR);
            $stmt->bindValue(':address', $post_address, \PDO::PARAM_STR);
            $stmt->bindValue(':phone', $post_phone, \PDO::PARAM_STR);
            $stmt->bindValue(':credit', $post_credit, \PDO::PARAM_STR);
            $stmt->execute();
        }
    }

    public function delete($post_id)
    {
        $sql = "delete from client where id = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':id', $post_id, \PDO::PARAM_INT);
        $stmt->execute();
    }

    public function search($post_name){
        $sql = "select id, name, address, phone, credit from client where name like :name";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':name', '%' . $post_name . '%', \PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }
}
